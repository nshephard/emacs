;;; BIOME SETTINGS
;;; https://github.com/SqrtMinusOne/biome
(use-package biome
  :ensure t
  :config
  (setq biome-query-coords
	'("Heeley, Sheffield" 53.35603 -1.46343)
	("High Neb, Hathersage" 53.36450 -1.65860)))
