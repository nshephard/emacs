;; JAVASCRIPT/JSON CONFIGURATION
;; --------------------------------------
(add-to-list 'auto-mode-alist '("\\.js" . js-mode))
(add-to-list 'auto-mode-alist '("\\.json" . json-mode))
(add-to-list 'auto-mode-alist '("\\.svelte" . js-ts-mode))
