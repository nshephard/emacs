;;; Various random modes not worthy of their own configuration

;; https://github.com/OpenSauce04/portage-modes
(use-package portage-modes
  :ensure t)

;; https://github.com/nverno/tmux-mode
(use-package tmux-mode
  :ensure t)
