;;; Kitty Keyborad Protocol in Emacs
;;; --------------------------------
;;; https://github.com/benjaminor/kkp
(use-package kkp
  :ensure t
  :config
  ;; (setq kkp-alt-modifier 'alt) ;; use this if you want to map the Alt keyboard modifier to Alt in Emacs (and not to Meta)
  (global-kkp-mode +1))
