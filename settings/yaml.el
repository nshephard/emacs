;; YAML CONFIGURATION
;; --------------------------------------
(use-package yaml-mode
  :init
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
  (add-to-list 'auto-mode-alist '("\\.cff\\'" . yaml-mode)))
