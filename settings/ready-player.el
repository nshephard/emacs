;; Ready Player Mode
;;
;; Preview media files in Emacs
;;
;; https://github.com/xenodium/ready-player
(use-package ready-player
  :ensure t
  :defer 0.5
  :config
  (ready-player-mode +1))
