;;; https://gitlab.com/fledermaus/totp.el
;;; https://www.masteringemacs.org/article/securely-generating-totp-tokens-emacs
(use-package totp-auth
  :ensure t
  :defer 0.5)

;;; https://github.com/juergenhoetzel/emacs-totp
;; (use-package totp
;;   :ensure t
;;   :defer 0.5)
