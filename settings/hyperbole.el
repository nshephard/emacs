;;; HYPERBOLE SETTINGS
;;; ==================
;;; https://www.gnu.org/software/hyperbole/
;;;
;;; https://www.youtube.com/watch?v=BrTpTNEXMyY
(use-package hyperbole
  :defer t
  :config
  (hyperbole-mode t))
