;;; https://codeberg.org/emacs-weirdware/scratch
;;;
;;; C-u M-x scratch prompts for which buffer mode to invoke
(use-package scratch
  :ensure t
  :bind ("C-c s" . scratch))
