;;; Completion frameworks
;;; Configuration of vertico, consult, marginalia, embark and orderless
;;;
;;; https://github.com/minad/vertico
;;; https://github.com/minad/marginalia
;;; https://github.com/minad/consult
;;; https://github.com/oantolin/embark
;;; https://github.com/oantolin/orderless

;; https://github.com/minad/vertico
(use-package vertico
  :ensure t
  :init
  (vertico-mode)
  :config
  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates
  ;; (setq vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  ;; (setq vertico-resize t)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  ;; (setq vertico-cycle t)

  ;; TAB completion - completes path selection rather than selecting current point
  ;; (keymap-set vertico-map "TAB" #'minibuffer-complete)
  )


;; https://github.com/oantolin/embark/
(use-package embark
  :ensure t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
  :init
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)
  ;; (add-to-list 'vertico-multiform-categories '(embark-keybinding grid))
  (setq embark-indicators
	'(embark-minimal-indicator  ; default is embark-mixed-indicator
	  embark-highlight-indicator
	  embark-isearch-highlight-indicator))
  ;; Show the Embark target at point via Eldoc. You may adjust the
  ;; Eldoc strategy, if you want to see the documentation from
  ;; multiple providers. Beware that using this can be a little
  ;; jarring since the message shown in the minibuffer can be more
  ;; than one line, causing the modeline to move up and down:
  ;; (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)
  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))
;; Consider https://github.com/oantolin/embark/wiki/Additional-Configuration#use-which-key-like-a-key-menu-prompt

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :ensure t
  :init
  (savehist-mode))


;; https://github.com/oantolin/orderless
(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(orderless basic)
	completion-category-defaults nil
	completion-category-overrides '((file (styles basic partial-completion)))))

;; https://github.com/minad/marginalia/
;; Enable rich annotations using the Marginalia package
(use-package marginalia
  :ensure t
  ;; Bind `marginalia-cycle' locally in the minibuffer.  To make the binding
  ;; available in the *Completions* buffer, add it to the
  ;; `completion-list-mode-map'.
  :bind
  (:map minibuffer-local-map
	("M-A" . marginalia-cycle))
  (:map completion-list-mode-map
	("M-A" . marginalia-cycle))
  ;; The :init section is always executed.
  :init

  ;; Marginalia must be activated in the :init section of use-package such that
  ;; the mode gets enabled right away. Note that this forces loading the
  ;; package.
  (marginalia-mode))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))


;; https://github.com/iyefrat/all-the-icons-completion
(use-package all-the-icons-completion
  :ensure t
  :hook
  (maginalia-mode-hook . all-the-icons-copmletion-marginalia-setup))

;;; https://company-mode.github.io/manual/
(use-package company
  :ensure t
  :defer 0.5
  :config
  (setq company-minimum-prefix-length 3)
  (setq company-idle-delay 0.3)
  (setq company-selection-wrap-around t)
  (setq company-dabbrev-other-buffers t)
  (setq company-dabbrev-code-other-buffers t)
  :hook
  (text-mode . company-mode)
  (prog-mode . company-mode)
  (org-src-mode . company-mode)
  ;; https://themagitian.github.io/posts/emacsconfig/
  :custom
  (custom-set-faces
   '(company-tooltip ((t (:background "#3e4452"))))
   '(company-tooltip-selection ((t (:background "#454c59"))))
   '(company-tooltip-common ((t (:background "#3e4452"))))
   '(company-scrollbar-bg ((t (:background "#282c34"))))))
