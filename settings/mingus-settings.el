;;; Mingus CONFIGURATION
;;; --------------------------------------
;;;
;;; GitHub : https://github.com/pft/mingus
(use-package mingus
  :config
  (setq mingus-mpd-host '192.168.1.28
        mingus-mpd-port '6600))
