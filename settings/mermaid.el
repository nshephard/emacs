;;; Mermaid Mode --- Settings for https://mermaid.js.org/
;;; https://github.com/abrochard/mermaid-mode/
(use-package mermaid-mode
  :ensure t)
(use-package ob-mermaid
  :ensure t)
;;; https://github.com/JonathanHope/mermaid-ts-mode
(use-package mermaid-ts-mode
  :ensure t)
