;; https://github.com/purcell/emacs-nixfmt
(use-package nixfmt
  :ensure t)

;; https://github.com/nix-community/nix-ts-mode
(use-package nix-ts-mode
  :ensure t
  :mode "\\.nix\\'")

;; https://github.com/t4ccer/agenix.el
(use-package agenix
  :ensure t
  :mode "\\.age\\'")
