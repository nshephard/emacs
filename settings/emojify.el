;;; EMOJIFY CONFIGURATION
;;; --------------------------------------
;;; https://github.com/iqbalansari/emacs-emojify
(use-package emojify
  :hook (after-init . global-emojify-mode))
