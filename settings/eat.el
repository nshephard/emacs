;; EAT CONFIGURATION
;; --------------------------------------
;; https://codeberg.org/akib/emacs-eat/
(use-package eat
  :ensure t)
