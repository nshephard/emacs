;;; ORG-NOTIFICATIONS CONFIGURATION
;;; --------------------------------------
;;;
;;; https://github.com/doppelc/org-notifications
(use-package org-notifications
  :ensure t
  :config
  (setq org-notifications-title "You have something to do..."))
